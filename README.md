archlinux-buildbot
==================

Buildbot instance for Arch Linux

[https://buildbot.pkgbuild.com/#/](https://buildbot.pkgbuild.com/#/)

[https://github.com/buildbot/buildbot](https://github.com/buildbot/buildbot)


# Goals
- Automatic package building
- Dependant rebuilds
- Automatic rebuilds for different architectures
- Automatic repository management for staging repositories
- Full integration tests. We shouldn't have to run this setup to test it.

We are not aiming for these features currently:
- Persistent artifacts
- Package signing (should be separate thing)

And probably more...
